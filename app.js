// ------------------------------------------
// Built in constructors
// ------------------------------------------

// String

const name1 = 'Jeff';
const name2 = new String('Jeff');

console.log('string type: ', typeof name1);
console.log('object type: ', typeof name2);


if (name2 === 'Jeff') {
  console.log('YES');
} else {
  console.log('NO');
}

// Number
const num1 = 5;
const num2 = new Number(5);

console.log('number type: ', typeof num1);
console.log('object type: ', typeof num2);

// Boolean

const bool1 = true;
const bool2 = new Boolean(true);

console.log('boolean type: ', typeof bool1);
console.log('object type: ', typeof bool2);

// Function

const getSum1 = function (x, y) {
  return x + y;
}

const getSum2 = new Function('x', 'y', 'return x + y');

console.log('function type: ', typeof getSum1);
console.log('function type: ', typeof getSum2);

// Object
const john = { name: 'John' };
const john2 = new Object({ name: 'John' });

console.log('object type: ', typeof john);
console.log('object type: ', typeof john2);

// Arrays
const arr1 = [1, 2, 3, 4];
const arr2 = new Array(1, 2, 3, 4);

console.log('output: ', arr1);
console.log('output: ', arr2);

// Regular Expression
const re1 = /\w+/;
const re2 = new RegExp('\\w+'); // You have to escape it with a backslash

console.log('output: ', re1);
console.log('output: ', re2);

// new has a slower execution time, avoid.

// ------------------------------------------
// Constructors & the 'this' keyword
// ------------------------------------------

function Person(firstName, lastName, dob) {
  this.firstName = firstName;
  this.lastName = lastName;
  this.birthday = new Date(dob);
  // this.calculateAge = function () {
  //   const diff = Date.now() - this.birthday.getTime();
  //   const ageDate = new Date(diff);
  //   return Math.abs(ageDate.getUTCFullYear() - 1970);
  // }
}

// ------------------------------------------
// Prototypes
// ------------------------------------------

// Object.prototype
// Person.prototype

const jack = new Person('Jack', 'Flower', '8-12-90');
const mary = new Person('Mary', 'Johnson', 'March 20 1978');

// Calculate age
Person.prototype.calculateAge = function () {
  const diff = Date.now() - this.birthday.getTime();
  const ageDate = new Date(diff);
  return Math.abs(ageDate.getUTCFullYear() - 1970);
}

// Get full name
Person.prototype.getFullName = function () {
  return `${this.firstName} ${this.lastName}`;
}

// Gets Married
Person.prototype.getsMarried = function (newLastName) {
  this.lastName = newLastName;
}

console.log(mary.calculateAge());
console.log(jack.calculateAge());
console.log(mary.getFullName());

mary.getsMarried('Smith');
console.log(mary.getFullName());

console.log(mary.hasOwnProperty('firstName')); // it's an actual property
console.log(mary.hasOwnProperty('getFullName')); // we can use that, but it's not an actual property

// ------------------------------------------
// Prototypal inheritance
// ------------------------------------------

// One object type inherit from another
// Guy object and Customer object that will inherit Person's prototype

// Guy constructor
function Guy(firstName, lastName) {
  this.firstName = firstName;
  this.lastName = lastName;
}

// Greeting
Guy.prototype.greeting = function () {
  return `Hello there ${this.firstName} ${this.lastName}.`
}

const guy1 = new Guy('Mel', 'Gibson');

console.log(guy1.greeting());

// Customer constructor
function Customer(firstName, lastName, phone, membership) {
  Guy.call(this, firstName, lastName);

  this.phone = phone;
  this.membership = membership;

  // not yet inheriting the prototype at this point - look below
}

// Inherit the Guy prototype methods
Customer.prototype = Object.create(Guy.prototype);

// Make Customer.prototype return Customer()
Customer.prototype.constructor = Customer;

// Create customer
const customer1 = new Customer('Tom', 'Smith', '555-555-5555', 'standard');

console.log(customer1);

// Example

console.log(customer1.greeting());

Customer.prototype.greeting = function () {
  return `Hello there ${this.firstName} ${this.lastName}. Welcome to our company.`;
}

console.log(customer1.greeting());

// ------------------------------------------
// Using Object.create
// ------------------------------------------

// Able to create prototypes inside of a parent object and then have different properties
// with different prototype methods (or different prototype functions).

const personPrototypes = {
  greeting: function () {
    return `Hello there ${this.firstName} ${this.lastName}`;
  },
  getsMarried: function (newLastName) {
    this.lastName = newLastName;
  }
}

const hannah = Object.create(personPrototypes);

hannah.firstName = 'Hannah';
hannah.lastName = 'Williams';
hannah.age = '30';

hannah.getsMarried('Thompson');

console.log(hannah.greeting());

const brad = Object.create(personPrototypes, {
  firstName: { value: 'Brad' },
  lastName: { value: 'Traversy' },
  age: { value: '36' },
});

console.log(brad);
console.log(brad.greeting());
