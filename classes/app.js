// ------------------------------------------
// ES6 Classes
// ------------------------------------------

class Person {
  constructor(firstName, lastName, dob) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.birthday = new Date(dob);
  }

  greeting() {
    return `Hello There ${this.firstName} ${this.lastName}`;
  }

  calculateAge() {
    const diff = Date.now() - this.birthday.getTime();
    const ageDate = new Date(diff);
    return Math.abs(ageDate.getUTCFullYear() - 1970);
  }

  getsMarried(newLastName) {
    this.lastName = newLastName;
  }


  // static methods - look below

  static addNumbers(x, y) {
    return x + y;
  }
}

const mary = new Person('Mary', 'Williams', '11-13-1980');

mary.getsMarried('Thompson');

console.log(mary);
console.log(mary.greeting());
console.log('age: ', mary.calculateAge());

// Static methods

// Can be used without instantiating an object
// (instantiate - when we create an object from a class)

// console.log(mary.addNumbers(1, 2)); // not a part of this instance
console.log(Person.addNumbers(1, 2));
